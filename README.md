# 強化(我們)學習：當AI玩起股票 #

講師：Mr. Owen:
現職：AI 工程師
經歷：
 - 台灣人工智慧學校 新竹四期技術班結業，第一名專題畢業(Playing Pikachu Volleyball with Reinforcement Learning)，最佳人氣獎畢業
 - 殿軍，西藥房團隊：探索新藥開藥
 - 專長：深度學習、生成模型、強化學習、網管、資安
 - 經驗：藉由生成模型探索新藥開發
 - 簡介：熱衷談論技術、像極了愛情
 
## 這堂課你可以學到什麼 ##
不同身份的你可以學到以下的事項
- 金融背景：了解 RL 如何運用在金融上、體驗寫 Tensorflow、體驗撰寫 RL 訓練的過程
- 工程師：增加 RL 實作經驗、練習實作 DQN
- 管理階層：如何跟工程師討論 RL、實作體驗寫 Tensorflow、體驗撰寫 RL 訓練的過程

## 最後 ##
Enjoy your class and have fun in playing AI!



